# Petit fetcher

## Setup du back

Pour initialiser le back (*à faire une seule fois*) :
```
cd rest_api/
npm install
```
Pour lancer le serveur :
```
nodemon start --ignore data.json
```

## Infos sur l'API

***GET***

>**URL du back :** localhost:3000

>**Avoir toutes les données :** /

>**Avoir les données d'un id donné :** /{id}

>**Avoir les données d'un prénom donné :** /firstname/{prenom}

>**Avoir les données d'un nom donné :** /lastname/{nom}

***POST***

>**Poster des données :** /new

Pour que le post de données fonctionne bien, il faut envoyer les données en JSON au format suivant :
```JS
{
    first_name : "string",
    last_name : "string",
    email : "string",
    gender : "string",
    ip_address : "string"
}
```

## Exercice

L'exercice est divisé en trois parties :

La première consiste à afficher toutes les données, servies depuis le serveur, sur la page index.html.
Ceci doit être fait de façon asynchrone via l'**API Fetch** de Javascript.

Ensuite, vous devez faire en sorte que lors d'une entrée dans le champ de recherche, une requête soit faite sur le serveur (toujours de façon asynchrone via l'**API Fetch**) et mette à jour la vue.

Enfin, vous devez prendre en charge l'ajout de données sur le serveur (toujours de façon asynchrone via l'**API Fetch**). Pour ceci, la vue sera sur le fichier new.html.