const express = require('express')
const cors = require('cors')
const app = express()
const port = 3000
const fs = require('fs')
const bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(cors())

function serializeData(data, code, err) {
    if (code == 200) {
        return {
            status: code,
            data: Array.isArray(data) ? data : [data]
        }
    } else {
        return {
            status: code,
            error: err,
            data: []
        }
    }
}


// --- GET : All data


app.get('/', (req, res) => {
    let jsonData = require('./data.json')

    res.status(200).send(serializeData(jsonData, 200))
})


// --- GET : A specific ID


app.get('/:id', (req, res) => {
    let jsonData = require('./data.json')

    let dataToServe = jsonData.find(element => element.id == req.params.id)

    if (dataToServe) {
        res.status(200).send(serializeData(dataToServe, 200))
    } else {
        res.status(404).send(serializeData(dataToServe, 404, "Aucune entrée n'a été trouvée."))
    }
})


// --- GET : A specific first name


app.get('/firstname/:name', (req, res) => {
    let jsonData = require('./data.json')

    let dataToServe = jsonData.filter(element => element.first_name?.toLowerCase().includes(req.params.name.toLowerCase()))

    if (dataToServe) {
        res.status(200).send(serializeData(dataToServe, 200))
    } else {
        res.status(404).send(serializeData(dataToServe, 404, "Aucune entrée n'a été trouvée."))
    }
})


// --- GET : A specific first name


app.get('/lastname/:name', (req, res) => {
    let jsonData = require('./data.json')

    let dataToServe = jsonData.filter(element => element.last_name?.toLowerCase().includes(req.params.name.toLowerCase()))

    if (dataToServe) {
        res.status(200).send(serializeData(dataToServe, 200))
    } else {
        res.status(404).send(serializeData(dataToServe, 404, "Aucune entrée n'a été trouvée."))
    }
})


// --- POST : Add an entry


app.post('/new', (req, res) => {
    let dataToAdd = req.body

    fs.readFile('./data.json', (err, data) => {
        let fileData = JSON.parse(data)

        let previousId = fileData[fileData.length - 1].id

        fileData.push({
            id: previousId + 1,
            ...dataToAdd
        })

        fs.writeFile('./data.json', JSON.stringify(fileData), (err) => {
            if (err) res.status(500).send("Une erreur est survenue, veuillez réessayer ultérieurement.")
        })
    })

    res.status(200).send("Ok")
})


// App listening


app.listen(port, () => {
    console.log(`App listening on port ${port}`)
})